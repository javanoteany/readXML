package utils;

/**
 * @author zjzhou
 * @version 1.0
 *
 * JDOM方式读取xml文件
 *
 * 简化与XML的交互并且比使用DOM实现更快,
 * 仅使用具体类而不使用接口因此简化了API,并且易于使用
 */

import java.io.FileInputStream;
import java.io.File;
import java.io.InputStream;

import java.util.List;

import org.jdom2.input.SAXBuilder;




import org.jdom2.*;

public class JDOMReadXML {

    public static void main(String[] args) throws Exception {
        //1.创建SAXBuilder对象
        SAXBuilder saxBuilder = new SAXBuilder();
        //2.创建输入流
        InputStream is = new FileInputStream(new File("src/main/resources/Books.xml"));
        //3.将输入流加载到build中
        Document document = saxBuilder.build(is);
        //4.获取根节点
        Element rootElement = document.getRootElement();
        //5.获取子节点
        List<Element> children = rootElement.getChildren();
        for (Element child : children) {
            System.out.println("通过rollno获取属性值:"+child.getAttribute("rollno"));
            List<Attribute> attributes = child.getAttributes();
            //打印属性
            for (Attribute attr : attributes) {
                System.out.println(attr.getName()+":"+attr.getValue());
            }
            List<Element> childrenList = child.getChildren();
            System.out.println("======获取子节点-start======");
            for (Element o : childrenList) {
                System.out.println("节点名:"+o.getName()+"---"+"节点值:"+o.getValue());
            }
            System.out.println("======获取子节点-end======");
        }
    }
}
