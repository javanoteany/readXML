package utils;

/**
 * @author zjzhou
 * @version 1.0
 *
 * 基于事件驱动,逐条解析,适用于只处理xml数据，
 * 不易编码,而且很难同时访问同一个文档中的多处不同数据
 */

import bean.Book;
import handler.SAXParseHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.List;

public class SAXReadXMLBean {

    private static List<Book> books = null;

    public List<Book> getBooks(String fileName) throws Exception{
        SAXParserFactory sParserFactory = SAXParserFactory.newInstance();
        SAXParser parser = sParserFactory.newSAXParser();

        SAXParseHandler handler = new SAXParseHandler();
        parser.parse(fileName, handler);

        return handler.getBooks();

    }

    public static void main(String[] args) {
        try {
            books = new SAXReadXMLBean().getBooks("src/main/resources/Books.xml");
            for(Book book:books){
                System.out.println(book);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}