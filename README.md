#  XML格式文件详解及Java解析XML文件内容方法

### XML格式文件详解

#### 1.概述

XML，即可扩展标记语言，XML是互联网数据传输的重要工具，它可以**跨越互联网任何的平台**，**不受编程语言和操作系统的限制**，可以说它是一个拥有互联网最高级别通行证的数据携带者。XML是当前处理结构化文档信息中相当给力的技术，XML有助于在服务器之间穿梭结构化数据，使得开发人员更加得心应手的控制数据的**存储**和**传输**。XML用于标记电子文件使其具有结构性的标记语言，可以用来**标记数据**、**定义数据类型**，是一种允许用户对自己的标记语言进行定义的源语言。XML是标准通用标记语言的子集，非常适合Web传输。XML提供统一的方法来描述和交换独立于应用程序或供应商的结构化数据。

 一个XML格式文件必须要有第一行的声明和它的文档元素的描述信息。

#### 2.XML的特点

XML与操作系统、编程语言的开发平台都无关；

实现不同系统之间的数据交互。



#### **3.XML的作用**

配置应用程序和网站；在配置文件里边所有的配置文件都是以XML的格式来编写的。

数据交互；跨平台进行数据交互，它可以跨操作系统，也可以跨编程语言的平台。



#### 4.XML声明

XML声明一般是XML文档的第一行；

version属性：用于说明当前XML文档的版本，因为都是在用1.0，所以这个属性值大家都写1.0，version属性是必须的；

encoding属性：用于说明当前XML文档使用的字符编码集，XML解析器会使用这个编码来解析XML文档。encoding属性是可选的，默认为UTF-8。注意，如果当前XML文档使用的字符编码集是GB2312，而encoding属性的值为UTF-8，那么一定会出错的；

standalone属性：用于说明当前XML文档是否为独立文档，如果该属性值为yes，表示当前XML文档是独立的，如果为no表示当前XML文档不是独立的，即依赖外部的约束文件。默认是yes

没有XML文档声明的XML文档，不是格式良好的XML文档；

XML文档声明必须从XML文档的1行1列开始。



#### 5.XML元素

##### XML元素的格式1

​	XML元素包含：开始标签、元素体（内容）、结束标签。例如：<hello>大家好</hello>

​	空元素：空元素只有开始标签，没有元素体和结束标签，但空元素一定要闭合。例如：<hello/>

##### XML元素的格式2

​	XML元素可以包含子元素或文本数据。例如：<a><b>hello</b></a>，a元素的元素体内容是b元素，而b元素的元素体内容是文本数据hello。

​	XML元素可以嵌套，但必须是合法嵌套。例如：<a><b>hello<a></b>就是错误的嵌套。

##### XML文档的根元素

​	格式良好的XML文档必须且仅有一个根元素！它是XML文档里面唯一的；它的开始是放在最前面，结束是放在最后面。



(1) 所有的XML元素都必须有结束标签；

(2) XML标签对大小写敏感；

(3)  XML必须正确地嵌套；

(4) 元素的命名规则：

​	名称中可以包含字母、数字或者其他的字符；

​	名称不能以数字或者标点符号开始；

​	名称中不能包含空格。

(5) 空元素

 

##### 元素命名规范

​	XML元素名可以包含字母、数字以及一些其它可见字符，但必须遵循下面的一些规范：

​	区分大小写：<a>和<A>是两个元素；

​	不能以数字开头：<1a>都是错误的；

​	最好不要以XML开头：<xml>、<Xml>、<XML>；

​	不能包含空格；

##### 元素属性


​	属性由属性名与属性值构成，中间用等号连接；

​	属性值必须使用引号括起来，单引或双引；

​	定义属性必须遵循与标签名相同的命名规范；

​	属性必须定义在元素的开始标签中；

​	一个元素中不能包含相同的属性名；

​	语法，<元素名 属性名=“属性值”/>

##### 注释

​	注释以<!--开头，以-->结束；

​	注释内容中不要出现”--”;

​	不要把注释放在标签中间；

​	注释不能嵌套。

```
<?xml version="1.0" encoding="UTF-8"?>   <!--声明-->
<bookstore>   <!--根元素-->
    <book id="1">
        <name>Thinking in Java (4th Edition)</name>
        <author>Bruce Eckel  </author>
        <year>2007</year>
        <price>75.60</price>
    </book>
    <book id="2">
        <name>Introduction to Algorithms </name>
        <author>Thomas H.Cormen </author>
        <year>2012</year>
        <price>89.60</price>
    </book>
</bookstore>
```




### XML格式文件解析方法

#### DOM

​	基于DOM解析的XML分析器是将其转换为一个对象模型的集合，用树这种数据结构对信息进行储存。通过DOM接口，应用程序可以在任何时候访问XML文档中的任何一部分数据，因此这种利用DOM接口访问的方式也被称为随机访问。这种方式也有缺陷，因为DOM分析器将整个XML文件转换为了树存放在内存中，当文件结构较大或者数据较复杂的时候，这种方式对内存的要求就比较高，且对于结构复杂的树进行遍历也是一种非常耗时的操作。不过DOM所采用的树结构与XML存储信息的方式相吻合，同时其随机访问还可利用，所以DOM接口还是具有广泛的使用价值。

​	**优点**：

​	形成了树结构，直观好理解，代码容易编写；

​	解析过程中树结构保存在内存中，方便修改。

​	**缺点**：

​	当XML文件较大时，对内存耗费比较大，容易影响解析性能并造成内存溢出。
    
     核心代码

```
 //Element方式
    public static void element(NodeList list){
        for (int i = 0; i <list.getLength() ; i++) {
            Element element = (Element) list.item(i);
            NodeList childNodes = element.getChildNodes();
            for (int j = 0; j <childNodes.getLength() ; j++) {
                if (childNodes.item(j).getNodeType()== Node.ELEMENT_NODE) {
                    //获取节点
                    System.out.print(childNodes.item(j).getNodeName() + ":");
                    //获取节点值
                    System.out.println(childNodes.item(j).getFirstChild().getNodeValue());
                }
            }
        }
    }

    public static void node(NodeList list){
        for (int i = 0; i <list.getLength() ; i++) {
            Node node = list.item(i);
            NodeList childNodes = node.getChildNodes();
            for (int j = 0; j <childNodes.getLength() ; j++) {
                if (childNodes.item(j).getNodeType()==Node.ELEMENT_NODE) {
                    System.out.print(childNodes.item(j).getNodeName() + ":");
                    System.out.println(childNodes.item(j).getFirstChild().getNodeValue());
                }
            }
        }
    }

    public static void main(String[] args) {
        //1.创建DocumentBuilderFactory对象
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        //2.创建DocumentBuilder对象
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document d = builder.parse("src/main/resources/Books.xml");
            NodeList sList = d.getElementsByTagName("book");
            //element(sList);
            node(sList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
```


#### SAX

​	SAX是一种XML解析的替代方法。相比于文档对象模型DOM，SAX 是读取和操作 XML 数据的更快速、更轻量的方法。SAX 允许您在读取文档时处理它，从而不必等待整个文档被存储之后才采取操作。它不涉及 DOM 所必需的开销和概念跳跃。 SAX API是一个基于事件的API ，适用于处理数据流，即随着数据的流动而依次处理数据。SAX API 在其解析您的文档时发生一定事件的时候会通知您。在您对其响应时，您不作保存的数据将会 被抛弃。 

​	**优点**：

​	采用事件驱动模式，对内存耗费比较小；

​	适用于只需要处理XML数据时。

​	**缺点**：

​	不易编码；

​	很难同时访问同一个XML中的多处数据。

 
     核心代码

```
public static void main(String[] args) throws Exception {
        //1.或去SAXParserFactory实例
        SAXParserFactory factory = SAXParserFactory.newInstance();
        //2.获取SAXparser实例
        SAXParser saxParser = factory.newSAXParser();
        //创建Handel对象
        SAXDemoHandel handel = new SAXDemoHandel();
        saxParser.parse("src/main/resources/Books.xml",handel);
    }
}

class SAXDemoHandel extends DefaultHandler {
    //遍历xml文件开始标签
    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        System.out.println("sax解析开始");
    }

    //遍历xml文件结束标签
    @Override
    public void endDocument() throws SAXException {
        super.endDocument();
        System.out.println("sax解析结束");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        if (qName.equals("book")){
            System.out.println("============开始遍历student=============");
            //System.out.println(attributes.getValue("rollno"));
        }
        else if (!qName.equals("book")&&!qName.equals("bookstore")){
            System.out.print("节点名称:"+qName+"----");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (qName.equals("book")){
            System.out.println(qName+"遍历结束");
            System.out.println("============结束遍历student=============");
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        String value = new String(ch,start,length).trim();
        if (!value.equals("")) {
            System.out.println(value);
        }
    }
```


#### JDOM

​	JDOM是一个开源项目，基于树型结构，利用纯JAVA的技术对XML文档实现解析、生成、序列化以及多种操作。

JDOM 直接为JAVA编程服务。它利用更为强有力的JAVA语言的诸多特性（方法重载、集合概念以及映射），把SAX和DOM的功能有效地结合起来。JDOM是用Java语言读、写、操作XML的新API函数。在使用设计上尽可能地隐藏原来使用XML过程中的复杂性。

​	**优缺点**：

​	仅使用具体类而不使用具体接口；

​	API中大量使用了Collections类。


      核心代码

```
public static void main(String[] args) throws Exception {
        //1.创建SAXBuilder对象
        SAXBuilder saxBuilder = new SAXBuilder();
        //2.创建输入流
        InputStream is = new FileInputStream(new File("src/main/resources/Books.xml"));
        //3.将输入流加载到build中
        Document document = saxBuilder.build(is);
        //4.获取根节点
        Element rootElement = document.getRootElement();
        //5.获取子节点
        List<Element> children = rootElement.getChildren();
        for (Element child : children) {
            System.out.println("通过rollno获取属性值:"+child.getAttribute("rollno"));
            List<Attribute> attributes = child.getAttributes();
            //打印属性
            for (Attribute attr : attributes) {
                System.out.println(attr.getName()+":"+attr.getValue());
            }
            List<Element> childrenList = child.getChildren();
            System.out.println("======获取子节点-start======");
            for (Element o : childrenList) {
                System.out.println("节点名:"+o.getName()+"---"+"节点值:"+o.getValue());
            }
            System.out.println("======获取子节点-end======");
        }
    }
```


#### DOM4j

​	DOM4J是一个Java的XML API，类似于JDOM，用来读写XML文件的。DOM4J是一个非常非常优秀的Java XML API，具有性能优异、功能强大和极端易用使用的特点，同时它也是一个开放源代码的软件。对主流的Java XML API进行的性能、功能和易用性的评测，DOM4J无论在那个方面都是非常出色的。如今你可以看到越来越多的Java软件都在使用DOM4J来读写XML，例如Hibernate，包括sun公司自己的JAXM也用了Dom4j。

​	**优缺点**：

​	JDOM的一种智能分支，合并了许多超出基本XML文档表示的功能；

​	使用接口和抽象基本类方法，是一个优秀的Java XML API；

​	具有性能优异，灵活性好、功能强大和极端易使用的特点；目前使用较多。 

    核心代码

        



    



```
 public static void main(String[] args) throws Exception {
        //1.创建Reader对象
        SAXReader reader = new SAXReader();
        //2.加载xml
        Document document = reader.read(new File("src/main/resources/Books.xml"));
        //3.获取根节点
        Element rootElement = document.getRootElement();
        Iterator iterator = rootElement.elementIterator();
        while (iterator.hasNext()){
            Element stu = (Element) iterator.next();
            List<Attribute> attributes = stu.attributes();
            System.out.println("======获取属性值======");
            for (Attribute attribute : attributes) {
                System.out.println(attribute.getValue());
            }
            System.out.println("======遍历子节点======");
            Iterator iterator1 = stu.elementIterator();
            while (iterator1.hasNext()){
                Element stuChild = (Element) iterator1.next();
                System.out.println("节点名："+stuChild.getName()+"---节点值："+stuChild.getStringValue());
            }
        }
    }
```

